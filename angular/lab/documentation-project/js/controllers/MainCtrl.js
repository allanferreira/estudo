app
	.controller('MainCtrl', ['$scope', '$http', 'charsAPI', 'racesAPI', 'racesTypesAPI', 'serialGenerator', function($scope, $http, charsAPI, racesAPI, 
racesTypesAPI, serialGenerator) {

		$scope.chars = [];
		$scope.races = [];
		$scope.racesTypes = [];

		//api
		var loadChars = function(){
			charsAPI.getChars().success(function(data){
				$scope.chars = data.data;
			});
		}
		var loadRaces = function(){
			racesAPI.getRaces().success(function(data){
				$scope.races = data.data;
			});
		}
		var loadRacesTypes = function(){
			racesTypesAPI.getRacesTypes().success(function(data){
				$scope.racesTypes = data.data;
			});
		}
		loadChars();
		loadRaces();
		loadRacesTypes();

		$scope.addChar = function(char){
			char.serial = serialGenerator.generate();

			var newChar = $scope.chars;
			newChar.push(char);

		    charsAPI.saveChars(newChar).success(function(data) {
				delete $scope.char;
				$scope.formAddChar.$setPristine();
				loadChars();
		    });
		}

		$scope.deleteChar = function(chars){
			$scope.chars = chars.filter(function(char){
				if(!char.select) return char;
			});
		}
		
		$scope.orderBy = function(key){
			$scope.sortBaseKey = key;
			$scope.sortBaseOrder = !$scope.sortBaseOrder;
		}

		$scope.isSelectChar = function(chars){
			return chars.some(function(char){
				return char.select;
			});
		}

	}])