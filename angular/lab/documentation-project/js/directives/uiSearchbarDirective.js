app.directive("uiSearchbar", function(){
	return {
		templateUrl: "views/components/uiSearchbar.html", //uso inline -> template: "<div>aaa</div>"
		replace: true, //remove a tag pai. Funciona apenas com templates de apenas um elemento principal
		restrict: "E", // A -> Atributo, E -> Elemento, C -> Classe do Elemento, M -> Comentário do Elemento
		transclude: false, // true permite acessar textos escritos no interior da diretiva e coloca-los no template via ng-transclude
		scope: {
			/*
			model: @atributo
			model: =atributo
			Se ambos tiverem o mesmo nome basta colocar '@' ou '=' no atributo

			//'@' usa o scope criado
			//'=' usa o scope atual na implementação
			*/
			text: "@",
			modelCurrentScope: "="
		}
	}
});