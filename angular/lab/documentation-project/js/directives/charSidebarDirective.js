app.directive("charSidebar", function(){
	return {
		templateUrl: "views/templates/charSidebar.html", //uso inline -> template: "<div>aaa</div>"
		replace: true, //remove a tag pai. Funciona apenas com templates de apenas um elemento principal
		restrict: "E", // A -> Atributo, E -> Elemento, C -> Classe do Elemento, M -> Comentário do Elemento
		transclude: false, // true permite acessar textos escritos no interior da diretiva e coloca-los no template via ng-transclude
	}
});