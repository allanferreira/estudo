app.factory("racesTypesAPI", function($http){
	var _getRacesTypes = function(){
		return $http.get('api/racesTypes');
	}

	return {
		getRacesTypes: _getRacesTypes
	}
});