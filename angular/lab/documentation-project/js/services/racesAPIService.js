app.factory("racesAPI", function($http){
	var _getRaces = function(){
		return $http.get('api/races');
	}

	return {
		getRaces: _getRaces
	}
});