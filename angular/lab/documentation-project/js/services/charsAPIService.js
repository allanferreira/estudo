app.factory("charsAPI", function($http){
	var _getChars = function(){
		return $http.get('api/chars');
		//return $http.get('http://cdn.mechamoallan.com.br/api/chars.json');
	}
	var _saveChars = function(char){
		return $http({
	        url: 'api/chars',
	        method: "POST",
	        data: {"data": char},
	        header: 'Content-Type: application/json'
	    });
	}

	return {
		getChars: _getChars,
		saveChars: _saveChars
	}
});