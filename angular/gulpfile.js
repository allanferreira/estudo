var gulp        = require('gulp')
var stylus      = require('gulp-stylus')
var plumber     = require('gulp-plumber')
var jade        = require('gulp-jade')
var nodemon     = require('gulp-nodemon')
var browserSync = require('browser-sync').create();

gulp.task('stylus', function(){
	return gulp.src('lab/stylus/main.styl')
    	.pipe(plumber())
		.pipe(stylus())
	    .pipe(plumber.stop())
		.pipe(gulp.dest('lab/dist/css'))
})

gulp.task('jade', function() {
  gulp.src('./lab/jade/documentation-project/**/*.jade')
    .pipe(plumber())
    .pipe(jade({
        pretty: true
    }))
    .pipe(plumber.stop())
    .pipe(gulp.dest('./lab/documentation-project'))
  gulp.src('./lab/jade/ajax/**/*.jade')
    .pipe(plumber())
    .pipe(jade({
        pretty: true
    }))
    .pipe(plumber.stop())
    .pipe(gulp.dest('./lab/ajax'))
})


gulp.task('watch', function(){
	gulp.watch('lab/stylus/**/*', ['stylus']);
    gulp.watch('lab/jade/**/*.jade', ['jade']);

})

gulp.task('nodemon', function (cb) {
    var started = false;    
    return nodemon({
        script: 'app.js'
    }).on('start', function () {
        if (!started) {
            cb();
            started = true; 
        } 
    });
});

gulp.task('server',['nodemon'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:5000",
        browser: "google chrome",
        port: 7000,
    });
})