'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();
var router = express.Router();
var fs = require('fs');
var jsonfile = require('jsonfile');

app.use(cors());
app.options('*', cors());
app.use(express.static(__dirname + '/lab'));

app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));
app.use(bodyParser.text({ type: 'text/html' }));

app.use(function(req, res, next) {
    var oneof = false;
    if(req.headers.origin) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        oneof = true;
    }
    if(req.headers['access-control-request-method']) {
        res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
        oneof = true;
    }
    if(req.headers['access-control-request-headers']) {
        res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
        oneof = true;
    }
    if(oneof) {
        res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    }

    // intercept OPTIONS method
    if (oneof && req.method == 'OPTIONS') {
        res.send(200);
    }
    else {
        next();
    }
});

app.all('*', function(req, res, next) {
       res.header("Access-Control-Allow-Origin", "*");
       res.header("Access-Control-Allow-Headers", "X-Requested-With");
       res.header('Access-Control-Allow-Headers', 'Content-Type');
       next();
});

/*
app.get('*', function(req, res) {
    res.status(404).sendFile('./public/404.html');
});

app.get('/', function(req, res) {
    res.sendFile('./public/index.html');
});
*/

//api
app.get('/documentation-project/api/chars', function(req, res) {
    res.sendFile(__dirname+'/lab/documentation-project/api/chars.json');
});

app.get('/documentation-project/api/races', function(req, res) {
    res.sendFile(__dirname+'/lab/documentation-project/api/races.json');
});

app.get('/documentation-project/api/racesTypes', function(req, res) {
    res.sendFile(__dirname+'/lab/documentation-project/api/racesTypes.json');
});

app.post('/documentation-project/api/chars', function(req, res) {
	var obj = req.body;
	var file = __dirname+"/lab/documentation-project/api/chars.json";
	 
	jsonfile.writeFile(file, obj, function (err) {
	  console.error(err)
	});
});

app.listen(5000);